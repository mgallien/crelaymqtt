/**
 SPDX-FileCopyrightText: 2020 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

 SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

int main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[])
{
    return 0;
}
